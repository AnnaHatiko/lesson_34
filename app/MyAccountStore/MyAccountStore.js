'use strict';

angular
    .module('myApp')
    .factory('MyAccountStore', function() {

        const state = {
            user: {}
        };

        return {
            getUser()  {
                return state.user;
            },
            addUser(item) {
                state.user = {
                    name: item.name,
                    email: item.email,
                    phone: item.phone
                };
                console.log(state.user);
            },
            removeUser() {
                state.user = {};
            }
        };

    });